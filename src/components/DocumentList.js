import React from 'react';
import { connect } from 'react-redux';
import { callApi } from '../middleware/api';
import { addDocument } from '../store/actions';

class DocumentList extends React.Component {
    componentDidMount() {
        console.log(`DocumentList did load, fetch documents`);
        callApi().then(data => {
            console.log('App.js we got', data);
            this.props.dispatch(addDocument(data));     //Dispatch to store
        });
    }

    render() {
        const {documents} = this.props.state;
        console.log(documents.length);

                                                        //Render props to view
        return (
            <div>
            <h2>Documents {documents.length}</h2>
            <pre>
                {documents[0] && JSON.stringify(documents[0].data)}     
            </pre>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({                   //Connect store to props
    state
});

export default connect(mapStateToProps)(DocumentList);
