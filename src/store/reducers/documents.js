const documents = (state = [], action) => {
    switch (action.type) {
        case 'ADD_DOCUMENT':
            return [
                ...state,
                {
                    data: action.document
                }
            ]
        case 'REMOVE_DOCUMENT':
            return [...state].filter(document => document.id !== action.id);
        default:
            return state;
    }
}

export default documents