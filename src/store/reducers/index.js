import { combineReducers } from 'redux';
import documents from './documents';
import visibilityFilter from './visibilityFilter';

export default combineReducers({
    documents,
    visibilityFilter
});