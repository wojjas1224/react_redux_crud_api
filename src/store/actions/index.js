export const addDocument = document => ({
    type: 'ADD_DOCUMENT',
    document
});

export const removeDocument = id => ({
    type: 'REMOVE_DOCUMENT',
    id
});

//TODO: implement filter