import React, { Component } from 'react';
import DocumentList from './components/DocumentList';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <main>
          <DocumentList />
        </main>
      </div>
    );
  }
}

export default App;
