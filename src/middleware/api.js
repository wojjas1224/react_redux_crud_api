const API_URL = 'https://api.jsonbin.io';
const BIN_ID = '5bed74fc18a56238b6f6d576';
const API_KEY = process.env.REACT_APP_JSONBIN_API_KEY;

const MOCK_API = true;

let isFetching = false;

export const callApi = () => {
    const fullUrl = `${API_URL}/b/${BIN_ID}`;

    if (isFetching) {
        return new Promise(resolve => resolve);
    }

    isFetching = true;

    if (MOCK_API) {
        return new Promise((resolve) => {
            setTimeout(() => resolve({ title: "Test JSON added from JSONBIN's web interface, from Dashboard"}), 1500);
        });
    } else {
        return fetch(fullUrl, { headers: new Headers({ "secret-key": API_KEY }) }).then((resp) => {
            return resp.json();
        }).then(data => {
            return data;
        }).catch(err => {
            console.log(`ERROR`, err);
        }).finally(() => {
            isFetching = false;
        });
    }
}